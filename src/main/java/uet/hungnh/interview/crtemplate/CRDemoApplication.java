package uet.hungnh.interview.crtemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CRDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CRDemoApplication.class, args);
    }
}
