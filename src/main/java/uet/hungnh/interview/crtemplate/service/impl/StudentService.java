package uet.hungnh.interview.crtemplate.service.impl;

import org.springframework.stereotype.Service;
import uet.hungnh.interview.crtemplate.pojo.Student;
import uet.hungnh.interview.crtemplate.service.IStudentService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dungpx on 9/17/2018.
 */

@Service
public class StudentService implements IStudentService {


    @Override
    public List<Student> getAllStudent() {
        List<Student> studentList = new ArrayList<>();
        Student student = new Student();
        student.setId(1);
        student.setName("Bob");
        student.setAge(33);
        studentList.add(student);

        return studentList;
    }
}
