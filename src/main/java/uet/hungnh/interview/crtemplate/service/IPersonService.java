package uet.hungnh.interview.crtemplate.service;

import uet.hungnh.interview.crtemplate.pojo.Person;

import java.util.List;

public interface IPersonService {
    List<Person> getPersons();
}
