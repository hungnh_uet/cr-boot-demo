package uet.hungnh.interview.crtemplate.service;

import java.io.InputStream;

public interface IPersonExportService {
    InputStream exportPdfPersonList() throws Exception;
}
