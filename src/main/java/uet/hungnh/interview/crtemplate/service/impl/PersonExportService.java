package uet.hungnh.interview.crtemplate.service.impl;

import com.crystaldecisions.reports.sdk.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.application.OpenReportOptions;
import com.crystaldecisions.sdk.occa.report.exportoptions.ReportExportFormat;
import org.springframework.stereotype.Service;
import uet.hungnh.interview.crtemplate.pojo.Person;
import uet.hungnh.interview.crtemplate.pojo.Student;
import uet.hungnh.interview.crtemplate.service.IPersonExportService;
import uet.hungnh.interview.crtemplate.service.IPersonService;
import uet.hungnh.interview.crtemplate.service.IStudentService;

import java.io.InputStream;
import java.util.List;

@Service
public class PersonExportService implements IPersonExportService {
    private final IPersonService personService;
    private final IStudentService studentService;

    public PersonExportService(IPersonService personService, IStudentService studentService) {
        this.personService = personService;
        this.studentService = studentService;
    }

    @Override
    public InputStream exportPdfPersonList() throws Exception {
        ReportClientDocument reportClientDoc = new ReportClientDocument();

        reportClientDoc.open("templates/person_list.rpt", OpenReportOptions._openAsReadOnly);

        final List<Person> persons = personService.getPersons();
        List<Student> studentList = studentService.getAllStudent();
        reportClientDoc.getDatabaseController().setDataSource(persons, Person.class, "Person", "afa");
        reportClientDoc.getDatabaseController().setDataSource(studentList, Student.class, "student", "aasd");

        return reportClientDoc.getPrintOutputController().export(ReportExportFormat.PDF);
    }
}
