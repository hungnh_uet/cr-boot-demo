package uet.hungnh.interview.crtemplate.service;

import uet.hungnh.interview.crtemplate.pojo.Student;

import java.util.List;

/**
 * Created by dungpx on 9/17/2018.
 */
public interface IStudentService {

    List<Student> getAllStudent();
}
