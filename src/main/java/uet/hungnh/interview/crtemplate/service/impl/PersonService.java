package uet.hungnh.interview.crtemplate.service.impl;

import org.springframework.stereotype.Service;
import uet.hungnh.interview.crtemplate.pojo.Person;
import uet.hungnh.interview.crtemplate.service.IPersonService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class PersonService implements IPersonService {
    @Override
    public List<Person> getPersons() {
        List<Person> personList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();

        calendar.clear();
        calendar.set(1970, Calendar.FEBRUARY, 22);

        Person person = new Person();
        person.setPersonId(1);
        person.setLastName("Ant");
        person.setFirstName("Adam");
        calendar.clear();
        calendar.set(1954, Calendar.NOVEMBER, 3);
        person.setBirthDate(calendar.getTime());
        personList.add(person);

        person = new Person();
        person.setPersonId(2);
        person.setLastName("Boop");
        person.setFirstName("Betty");
        calendar.clear();
        calendar.set(1930, Calendar.AUGUST, 9);
        person.setBirthDate(calendar.getTime());
        personList.add(person);

        person = new Person();
        person.setPersonId(3);
        person.setLastName("Columbus");
        person.setFirstName("Christopher");
        calendar.clear();
        calendar.set(1451, Calendar.AUGUST, 27);
        person.setBirthDate(calendar.getTime());
        personList.add(person);

        return personList;
    }
}
