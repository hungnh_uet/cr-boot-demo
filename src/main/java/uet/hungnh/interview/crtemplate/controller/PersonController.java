package uet.hungnh.interview.crtemplate.controller;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uet.hungnh.interview.crtemplate.service.IPersonExportService;

import java.io.InputStream;

@RestController
@RequestMapping("/persons")
public class PersonController {

    private final IPersonExportService personExportService;

    public PersonController(IPersonExportService personExportService) {
        this.personExportService = personExportService;
    }

    @GetMapping("/pdf")
    public ResponseEntity<InputStreamResource> test() throws Exception {

        InputStream reportInputStream = personExportService.exportPdfPersonList();

        return ResponseEntity
                .ok()
                .header("Content-Disposition", "inline; filename=persons-report.pdf")
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(reportInputStream));
    }
}
